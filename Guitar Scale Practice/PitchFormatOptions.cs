﻿namespace Guitar_Scale_Practice
{
	public class PitchFormatOptions
	{
		public bool DisplayOctave { get; set; }
		//Will add option for accidentals and scales later on, at some point

		public static PitchFormatOptions Default => Octave;
		public static PitchFormatOptions Octave { get; } = new PitchFormatOptions { DisplayOctave = true };
		public static PitchFormatOptions NoOctave { get; } = new PitchFormatOptions { DisplayOctave = false };
	}
}