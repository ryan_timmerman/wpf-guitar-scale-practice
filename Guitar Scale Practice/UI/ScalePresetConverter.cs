﻿using Guitar_Scale_Practice.Models.Guitar.Presets;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace Guitar_Scale_Practice.UI
{
    public class ScalePresetConverter : IValueConverter
    {
        private Dictionary<ScalePreset, string> Names = new Dictionary<ScalePreset, string>()
        {
            {ScalePreset.Blues, "Blues" },
            {ScalePreset.Dorian, "Dorian" },
            {ScalePreset.HarmonicMinor, "Harmonic Minor" },
            {ScalePreset.Locrian, "Locrian" },
            {ScalePreset.Lydian, "Lydian" },
            {ScalePreset.Major, "Major" },
            {ScalePreset.Mixolydian, "Mixolydian" },
            {ScalePreset.NaturalMinor, "Natural Minor" },
            {ScalePreset.PentatonicMajor, "Pentatonic Major" },
            {ScalePreset.PentatonicMinor, "Pentatonic Minor" },
            {ScalePreset.Phrygian, "Phrygian" },
            {ScalePreset.PowerChord, "Power Chord" },
        };

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is ScalePreset scale)
            {
                return ConvertScale(scale);
            }
            else if(value is IEnumerable<ScalePreset> scales)
            {
                return scales.Select(s => ConvertScale(s));
            }
            else
            {
                Debugger.Break();
                return null;
            }
        }

        private string ConvertScale(ScalePreset preset)
        {
            if (Names.TryGetValue(preset, out string converted))
            {
                return converted;
            }
            else
            {
                return $"{preset} (?)";
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if(value is string description)
            {
                return Names.FirstOrDefault(x => x.Value == description).Key;
            }
            else
            {
                Debugger.Break();
                return null;
            }
        }
    }
}
