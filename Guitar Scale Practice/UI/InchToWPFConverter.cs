﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace Guitar_Scale_Practice.UI
{
	public class InchToWPFConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if(value is double inches)
			{
				return inches * 96; 
			}
            else if(value == DependencyProperty.UnsetValue)
            {
                return null;
            }
            else if(value == null)
            {
                return null;
            }
            else
			{
                Debugger.Break();
                return null;
				//throw new ArgumentException($"Value is not double: {value}");
			}
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
