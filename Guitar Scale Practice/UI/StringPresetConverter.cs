﻿using Guitar_Scale_Practice.Models.Guitar.Presets;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace Guitar_Scale_Practice.UI
{
    public class StringPresetConverter : IValueConverter
    {
        private Dictionary<InstrumentPreset, string> _Instruments = new Dictionary<InstrumentPreset, string>
        {
            {InstrumentPreset.Bass4, "Bass (4)" },
            {InstrumentPreset.Bass5, "Bass (5)" },
            {InstrumentPreset.Guitar6, "Guitar (6)" },
            {InstrumentPreset.Guitar7, "Guitar (7)" },
        };

        private Dictionary<TuningPreset, string> _Tunings = new Dictionary<TuningPreset, string>
        {
            {TuningPreset.EDropD, "Drop D" },
            {TuningPreset.EflatDropDflat, "E♭ Drop D♭" },
            {TuningPreset.EflatStandard, "E♭ Standard" },
            {TuningPreset.EStandard, "E Standard"},
        };

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is IEnumerable<StringPreset> presets)
            {
                return presets.Select(p => ConvertPreset(p));
            }
            if (value is StringPreset stringPreset)
            {
                return ConvertPreset(stringPreset);
            }
            else if (value is InstrumentPreset preset)
            {
                return ConvertInstrument(preset);
            }
            else if (value is TuningPreset tuning)
            {
                return ConvertTuning(tuning);
            }
            else
            {
                Debugger.Break();
                return null;
            }
        }

        private string ConvertPreset(StringPreset preset)
        {
            return $"{ConvertInstrument(preset.Instrument)} - {ConvertTuning(preset.Tuning)}";
        }

        private string ConvertInstrument(InstrumentPreset instrument)
        {
            if (_Instruments.TryGetValue(instrument, out string value))
            {
                return value;
            }
            else
            {
                return $"{instrument} (?)";
            }
        }

        private string ConvertTuning(TuningPreset tuning)
        {
            if (_Tunings.TryGetValue(tuning, out string value))
            {
                return value;
            }
            else
            {
                return $"{tuning} (?)";
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
