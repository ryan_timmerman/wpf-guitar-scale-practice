﻿using Guitar_Scale_Practice.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace Guitar_Scale_Practice.UI
{
	public class NoteNameConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if(value is AbsoluteNote n)
			{
				return NoteHelper.ToString(n);
			}
            else if(value is Pitch p)
            {
                return NoteHelper.ToString(p);
            }
            else if(value is IEnumerable<Pitch> pitches)
            {
                return pitches.Select(pitch => NoteHelper.ToString(pitch));
            }
            else if (value == DependencyProperty.UnsetValue)
            {
                return null;
            }
            else if (value == null)
            {
                return null;
            }
            else
			{
                Debugger.Break();
                return null;
                //throw new ArgumentException($"Value is not {nameof(AbsoluteNote)}: {value}");
			}
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
