﻿using Guitar_Scale_Practice.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Guitar_Scale_Practice
{
	public class NoteHelper
	{
		public static AbsoluteNote GetHalfStep(AbsoluteNote baseNote)
		{
			Pitch newPitch;
			int newOctave;

			if(baseNote.Pitch == Pitch.B)
			{
				newPitch = Pitch.C;
				newOctave = baseNote.Octave + 1;
			}
			else
			{
				newPitch = baseNote.Pitch + 1;
				newOctave = baseNote.Octave;
			}

			return new AbsoluteNote(newPitch, newOctave);
		}

		public static string ToString(AbsoluteNote note, PitchFormatOptions options = null)
		{
			if(options == null)
			{
				options = PitchFormatOptions.Default;
			}

			if(options.DisplayOctave)
			{
				return $"{ToString(note.Pitch, options)}{note.Octave}";
			}
			else
			{
				return $"{ToString(note.Pitch, options)}";
			}
		}

        public static string ToString(Pitch p, PitchFormatOptions options = null)
        {
            return SharpNoteTable[p];
        }

		private static Dictionary<Pitch, string> SharpNoteTable = new Dictionary<Pitch, string>
		{
			{Pitch.A, "A" },
			{Pitch.Asharp, "A#" },
			{Pitch.B, "B" },
			{Pitch.C, "C" },
			{Pitch.Csharp, "C#" },
			{Pitch.D, "D" },
			{Pitch.Dsharp, "D#" },
			{Pitch.E, "E" },
			{Pitch.F, "F" },
			{Pitch.Fsharp, "F#" },
			{Pitch.G, "G" },
			{Pitch.Gsharp, "G#" },
		};
	}
}
