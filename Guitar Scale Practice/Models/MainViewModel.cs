﻿using Guitar_Scale_Practice.Models.Guitar.Presets;
using PropertyChanged;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Guitar_Scale_Practice.Models
{
    using Guitar_Scale_Practice.Models.Guitar.Scales;
    using Guitar_Scale_Practice.Models.NoteDisplay;
    using static Pitch;
    using static ScalePreset;

    [ImplementPropertyChanged]
    public class MainViewModel
    {
        public Fretboard Frets { get; private set; }
        public FretboardDefinition FretboardDef { get; private set; }
        public double BoardWidthIn { get; set; } = 24;

        public NoteDisplayController NoteController { get; private set; }

        private FretAnimator _Animator;

		public MainViewModel()
		{
			FretboardDef = new FretboardDefinition
			{
				//Basically, my electric guitar in standard E tuning
				//I will create a bunch of predefined configs in the future
				//for guitars and other fretted and stringed instruments
				FretCount = 24,
				BoardWidth = BoardWidthIn,
				OpenNotes = new ObservableCollection<AbsoluteNote>
				{
					new AbsoluteNote(E, 4),
					new AbsoluteNote(B, 3),
					new AbsoluteNote(G, 3),
					new AbsoluteNote(D, 3),
					new AbsoluteNote(A, 2),
					new AbsoluteNote(E, 2),
				}
			};

			Frets = new Fretboard(FretboardDef);

            _Animator = new FretAnimator(Frets);

            NoteController = new NoteDisplayController(_Animator);
		}
	}
}
