﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Guitar_Scale_Practice.Models
{
    public class Command : ICommand
    {
        private Action _Action;
        private Func<bool> _CanExecute;

        public Command(Action action) : this(action, () => true)
        {

        }

        public Command(Action action, Func<bool> canExecute)
        {
            _Action = action;
            _CanExecute = canExecute;
        }

        public bool CanExecute(object parameter)
        {
            return _CanExecute();
        }

        public void Execute(object parameter)
        {
            if(CanExecute(parameter))
            {
                _Action();
            }
        }

        public event EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
            }
            remove
            {
                CommandManager.RequerySuggested -= value;
            }
        }
    }
}
