﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Guitar_Scale_Practice.Models.Guitar.Presets
{
    public enum InstrumentPreset
    {
        Guitar6,
        Guitar7,
        Bass4,
        Bass5,
    }
}
