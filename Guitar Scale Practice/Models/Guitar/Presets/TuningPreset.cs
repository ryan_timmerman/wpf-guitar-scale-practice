﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Guitar_Scale_Practice.Models.Guitar.Presets
{
    public enum TuningPreset
    {
        EStandard,
        EDropD,
        EflatStandard,
        EflatDropDflat,
    }
}
