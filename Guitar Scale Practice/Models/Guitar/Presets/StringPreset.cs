﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Guitar_Scale_Practice.Models.Guitar.Presets
{
    public class StringPreset
    {
        public InstrumentPreset Instrument { get; set; }
        public TuningPreset Tuning { get; set; }

        public static implicit operator StringPreset(ValueTuple<InstrumentPreset, TuningPreset> preset)
        {
            return new StringPreset
            {
                Instrument = preset.Item1,
                Tuning = preset.Item2,
            };
        }
    }
}
