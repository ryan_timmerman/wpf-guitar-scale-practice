﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Guitar_Scale_Practice.Models
{
    using Guitar_Scale_Practice.Models.Guitar.Presets;
    using System.Collections.ObjectModel;
    using static Pitch;
    using static Guitar.Presets.InstrumentPreset;
    using static Guitar.Presets.TuningPreset;

    public partial class FretboardDefinition
    {
        public static Dictionary<StringPreset, FretboardDefinition> Presets { get; }

        static FretboardDefinition()
        {
            //One goal of this project is to abuse C# 7 features, as you can no doubt see
            //Gotta love value tuples
            //Oh, and partial classes as well.  I'm experimenting, you see.
            Presets = new Dictionary<StringPreset, FretboardDefinition>();

            //String preset has an implicit conversion operator which makes these definitions possible
            //The terseness makes it a bit difficult to read though

            Define((Guitar6, EStandard), 24, (E, 4), (B, 3), (G, 3), (D, 3), (A, 2), (E, 2));
            Define((Guitar6, EDropD), 24, (E, 4), (B, 3), (G, 3), (D, 3), (A, 2), (D, 2));

            Define((Guitar6, EflatStandard), 24, (Dsharp, 4), (Asharp, 3), (Fsharp, 3), (Csharp, 3), (Gsharp, 2), (Dsharp, 2));
            Define((Guitar6, EflatDropDflat), 24, (Dsharp, 4), (Asharp, 3), (Fsharp, 3), (Csharp, 3), (Gsharp, 2), (Csharp, 2));

            Define((Guitar7, EStandard), 24, (E, 4), (B, 3), (G, 3), (D, 3), (A, 2), (E, 2), (C, 2));

            Define((Bass4, EStandard), 24, (G, 3), (D, 3), (A, 2), (E, 2));
            Define((Bass4, EDropD), 24, (G, 3), (D, 3), (A, 2), (D, 2));

            Define((Bass4, EflatStandard), 24, (Fsharp, 3), (Csharp, 3), (Gsharp, 2), (Dsharp, 2));
            Define((Bass4, EflatDropDflat), 24, (Fsharp, 3), (Csharp, 3), (Gsharp, 2), (Csharp, 2));
        }

        private static void Define(StringPreset preset, int fretCount, params (Pitch pitch, int octave)[] notes)
        {
            Presets.Add(preset, new FretboardDefinition
            {
                FretCount = fretCount,
                OpenNotes = DefineStrings(notes)
            });
        }

        private static ObservableCollection<AbsoluteNote> DefineStrings(params (Pitch pitch, int octave)[] notes)
        {
            var absoluteNotes = new ObservableCollection<AbsoluteNote>();

            foreach (var note in notes)
            {
                absoluteNotes.Add(new AbsoluteNote(note.pitch, note.octave));
            }

            return absoluteNotes;
        }

        public static IEnumerable<StringPreset> PresetNames => Presets.Keys;
    }
}
