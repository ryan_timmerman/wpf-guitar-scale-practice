﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Guitar_Scale_Practice.Models
{
	[DebuggerDisplay("{NoteHelper.ToString(this)}")]
	public class AbsoluteNote
	{
		public Pitch Pitch { get; set; }
		public int Octave { get; set; }

		public AbsoluteNote(Pitch pitch, int octave)
		{
			if((octave < 0) || (octave > 9))
			{
				throw new ArgumentException($"Octave {octave} is out of range");
			}

			Octave = octave;
			Pitch = pitch;
		}

		public override string ToString()
		{
			return NoteHelper.ToString(this);
		}
	}
}
