﻿namespace Guitar_Scale_Practice.Models
{
	//I know there is proper terminology for this but I never studied theory
	public enum ScaleMember
	{
        Outside,
        Root,
		Note,
		ExtraNote,
        Excluded,
	}
}