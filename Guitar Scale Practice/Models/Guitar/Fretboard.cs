﻿using PropertyChanged;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Guitar_Scale_Practice.Models
{
    [ImplementPropertyChanged]
    public class Fretboard
    {
        public ObservableCollection<GuitarString> Strings { get; private set; }

        public Fretboard(FretboardDefinition def)
        {
            def.SettingsApplied += Def_SettingsApplied;
            UpdateStrings(def);
        }

        private void Def_SettingsApplied(object sender, EventArgs e)
        {
            UpdateStrings(sender as FretboardDefinition);
        }

        private void UpdateStrings(FretboardDefinition def)
        {
            Strings = new ObservableCollection<GuitarString>();

            int stringCount = 1;
            foreach (var note in def.OpenNotes)
            {
                var gtrString = new GuitarString(def.FretCount, note, stringCount, def.BoardWidth);
                Strings.Add(gtrString);
                stringCount++;
            }

            var test = GetByPitch(Pitch.C);
        }

        public IEnumerable<GuitarFret> GetByPitch(Pitch p)
        {
            return Get(f => f.Note.Pitch == p);
        }

        public IEnumerable<GuitarFret> GetByOctave(int octave)
        {
            ValidateOctaveNumber(octave);

            return Get(f => f.Note.Octave == octave);
        }

        public IEnumerable<GuitarFret> GetByNote(Pitch p, int octave)
        {
            return GetByOctave(octave).Where(f => f.Note.Pitch == p);
        }

        public GuitarFret GetSpecificFret(Pitch p, int octave, int stringNbr)
        {
            if ((stringNbr < 0) || (stringNbr >= Strings.Count))
            {
                throw new ArgumentException($"Invalid string number: {stringNbr}");
            }

            ValidateOctaveNumber(octave);

            return Strings[stringNbr].Frets.FirstOrDefault(f =>
            {
                return (f.Note.Pitch == p) && (f.Note.Octave == octave);
            });
        }

        public IEnumerable<GuitarFret> GetAll()
        {
            return Get(f => true);
        }

        public IEnumerable<GuitarFret> Get(Func<GuitarFret, bool> condition)
        {
            foreach (var fret in Strings.SelectMany(s => s.Frets).Where(condition))
            {
                yield return fret;
            }
        }

        private void ValidateOctaveNumber(int octave)
        {
            if ((octave < 0) || (octave > 9))
            {
                throw new ArgumentException($"Invalid octave: {octave}");
            }
        }
    }
}
