﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Guitar_Scale_Practice.Models.Guitar.Scales
{
    public enum NoteInterval
    {
        PerfectUnison = 0,
        MinorSecond = 1,
        MajorSecond = 2,
        MinorThird = 3,
        MajorThird = 4,
        PerfectFourth = 5,
        Tritone = 6,
        PerfectFifth = 7,
        MinorSixth = 8,
        MajorSixth = 9,
        MinorSeventh = 10,
        MajorSeventh = 11,
        PerfectOctave = PerfectUnison,

        P1 = PerfectUnison,
        m2 = MinorSecond,
        M2 = MajorSecond,
        m3 = MinorThird,
        M3 = MajorThird,
        P4 = PerfectFourth,
        A4 = Tritone,
        P5 = PerfectFifth,
        m6 = MinorSixth,
        M6 = MajorSixth,
        m7 = MinorSeventh,
        M7 = MajorSeventh,
        P8 = PerfectOctave,
    }
}
