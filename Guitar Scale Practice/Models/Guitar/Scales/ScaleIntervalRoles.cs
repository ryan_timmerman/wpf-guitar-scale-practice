﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Guitar_Scale_Practice.Models.Guitar.Scales
{
    public class ScaleIntervalRoles
    {
        public List<NoteInterval> MemberNotes { get; set; }
        public List<NoteInterval> ExtraNotes { get; set; }

        public ScaleIntervalRoles()
        {
            MemberNotes = new List<NoteInterval>();
            ExtraNotes = new List<NoteInterval>();
        }
    }
}
