﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Guitar_Scale_Practice.Models.Guitar
{
    public class GuitarScale
    {
        public Pitch Root { get; set; }
        public List<Pitch> ScaleNotes { get; set; }
        public List<Pitch> ExtraNotes { get; set; }

        public GuitarScale()
        {
            ScaleNotes = new List<Pitch>();
            ExtraNotes = new List<Pitch>();
        }
    }
}
