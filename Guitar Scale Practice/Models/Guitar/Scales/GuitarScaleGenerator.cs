﻿using Guitar_Scale_Practice.Models.Guitar.Presets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Guitar_Scale_Practice.Models.Guitar.Scales
{
    using static ScalePreset;
    using static NoteInterval;

    public static class GuitarScaleGenerator
    {
        public static Dictionary<ScalePreset, ScaleIntervalRoles> ScaleIntervals { get; private set; }

        static GuitarScaleGenerator()
        {
            ScaleIntervals = new Dictionary<ScalePreset, ScaleIntervalRoles>();
            Create(Major, null, M2, M3, P4, P5, M6, M7);
            Create(Dorian, null, M2, m3, P4, P5, M6, m7);
            Create(Phrygian, null, m2, m3, P4, P5, m6, m7);
            Create(Lydian, null, M2, M3, A4, P5, M6, M7);
            Create(Mixolydian, null, M2, M3, P4, P5, M6, m7);
            Create(NaturalMinor, null, M2, m3, P4, P5, m6, m7);
            Create(Locrian, null, m2, m3, P4, A4, m6, m7);
            Create(PowerChord, null, P5);
            Create(HarmonicMinor, null, M2, m3, P4, P5, m6, M7);
            Create(PentatonicMinor, null, m3, P4, P5, m7);
            Create(PentatonicMajor, null, M2, M3, P5, M6);
            Create(Blues, new List<NoteInterval> { A4 }, m3, P4, P5, m7);
        }

        public static GuitarScale Generate(Pitch rootPitch, ScalePreset preset)
        {
            var scale = new GuitarScale();

            var roles = ScaleIntervals[preset];

            scale.Root = rootPitch;

            foreach(var member in roles.MemberNotes)
            {
                scale.ScaleNotes.Add(CreatePitch(rootPitch, member));
            }

            foreach(var extra in roles.ExtraNotes)
            {
                scale.ExtraNotes.Add(CreatePitch(rootPitch, extra));
            }

            return scale;
        }

        private static Pitch CreatePitch(Pitch rootPitch, NoteInterval member)
        {
            int rootIdx = (int)rootPitch;
            int noteIdx = rootIdx + (int)member;

            int intervalCount = Enum.GetValues(typeof(NoteInterval)).Cast<NoteInterval>().Distinct().Count();

            noteIdx %= intervalCount;

            return (Pitch)noteIdx;
        }

        //Root and octave are a given here
        private static void Create(ScalePreset scale, List<NoteInterval> extras = null, params NoteInterval[] members)
        {
            ScaleIntervalRoles roles = new ScaleIntervalRoles();

            if (extras != null)
            {
                roles.ExtraNotes.AddRange(extras);
            }

            roles.MemberNotes.AddRange(members);

            ScaleIntervals.Add(scale, roles);
        }
    }
}
