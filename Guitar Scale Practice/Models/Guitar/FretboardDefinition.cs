﻿using Guitar_Scale_Practice.Models.Guitar.Presets;
using PropertyChanged;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Guitar_Scale_Practice.Models
{
    [ImplementPropertyChanged]
    public partial class FretboardDefinition
    {
        public int FretCount { get; set; }
        public double BoardWidth { get; set; }
        public ObservableCollection<AbsoluteNote> OpenNotes { get; set; }
        public Command ApplySettingsCommand { get; private set; }

        public int PresetIndex { get; set; }

        public event EventHandler SettingsApplied;

        public FretboardDefinition()
        {
            FretCount = 24;
            OpenNotes = new ObservableCollection<AbsoluteNote>();
            ApplySettingsCommand = new Command(UpdateFromPreset);
        }

        private void UpdateFromPreset()
        {
            var def = Presets.ElementAt(PresetIndex).Value;
            OpenNotes = def.OpenNotes;
            FretCount = def.FretCount;

            SettingsApplied?.Invoke(this, EventArgs.Empty);
        }
    }


}
