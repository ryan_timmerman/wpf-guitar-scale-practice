﻿using PropertyChanged;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Guitar_Scale_Practice.Models
{
	[ImplementPropertyChanged]
	[DebuggerDisplay("{Note} - {ScaleMember}")]
	public class GuitarFret
	{
		public AbsoluteNote Note { get; }

		public ScaleMember ScaleMember { get; set; }

		public int FretNumber { get; private set; }
		public double FretWidthIn { get; set; }

		public GuitarFret(AbsoluteNote pitch, int fretNumber, double fretWidthIn)
		{
			Note = pitch;
			FretNumber = fretNumber;
			FretWidthIn = fretWidthIn;
            ScaleMember = ScaleMember.Outside;
		}
	}
}
