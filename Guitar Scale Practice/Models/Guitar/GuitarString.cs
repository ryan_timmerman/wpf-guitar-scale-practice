﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Guitar_Scale_Practice.Models
{
	public class GuitarString
	{
		public ObservableCollection<GuitarFret> Frets { get; private set; }
		public ObservableCollection<GuitarFret> StringFrets => new ObservableCollection<GuitarFret>(Frets.Skip(1));

		public int StringNumber { get; }

		public GuitarString(int numFrets, AbsoluteNote openNote, int stringNumber, double boardWidth)
		{
			if(numFrets < 1)
			{
				throw new ArgumentException($"Invalid number of frets on string: {numFrets}");
			}

			StringNumber = stringNumber;
			Frets = new ObservableCollection<GuitarFret>();

			double fretWidth = boardWidth;

			for(int i = 0; i < numFrets; i++)
			{
				var fretWidthIn = GetWidth(i + 1, boardWidth);
				Frets.Add(new GuitarFret(openNote, i, fretWidthIn));
				openNote = NoteHelper.GetHalfStep(openNote);
			}
		}

		private double GetWidth(int fretNumber, double boardWidth)
		{
			var dist1 = FretDistance(fretNumber, boardWidth);
			var dist2 = FretDistance(fretNumber - 1, boardWidth);
			return dist2 - dist1;
		}

		private double FretDistance(int fretNumber, double boardWidth)
		{
			return boardWidth / (Math.Pow(2, fretNumber / 12.0));
		}
	}
}
