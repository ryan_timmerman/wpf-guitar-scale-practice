﻿using Guitar_Scale_Practice.Models.Guitar.Presets;
using Guitar_Scale_Practice.Models.Guitar.Scales;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Guitar_Scale_Practice.Models.NoteDisplay
{
    public class FretAnimator
    {
        public Fretboard Board { get; set; }

        public FretAnimator(Fretboard board)
        {
            Board = board;
        }

        public void ClearAll()
        {
            foreach(var fret in Board.GetAll())
            {
                fret.ScaleMember = ScaleMember.Outside;
            }
        }

        public void ShowScale(Pitch root, ScalePreset scale)
        {
            ClearAll();
            var scaleNotes = GuitarScaleGenerator.Generate(root, scale);

            foreach(var rootFret in Board.GetByPitch(root))
            {
                rootFret.ScaleMember = ScaleMember.Root;
            }

            foreach(var memberNote in scaleNotes.ScaleNotes)
            {
                foreach(var memberFret in Board.GetByPitch(memberNote))
                {
                    memberFret.ScaleMember = ScaleMember.Note;
                }
            }

            foreach(var extraNote in scaleNotes.ExtraNotes)
            {
                foreach(var extraFret in Board.GetByPitch(extraNote))
                {
                    extraFret.ScaleMember = ScaleMember.ExtraNote;
                }
            }
        }
    }
}
