﻿using Guitar_Scale_Practice.Models.NoteDisplay;

namespace Guitar_Scale_Practice.Models
{
    public class NoteDisplayController
    {
        private FretAnimator _Animator;

        public ScaleHighlightOptions ScaleHighlight { get; set; }

        public NoteDisplayController(FretAnimator animator)
        {
            _Animator = animator;
            ScaleHighlight = new ScaleHighlightOptions(_Animator);
        }
    }
}