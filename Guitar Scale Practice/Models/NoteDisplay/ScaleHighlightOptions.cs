﻿using System;
using Guitar_Scale_Practice.Models.Guitar.Presets;
using PropertyChanged;
using Guitar_Scale_Practice.Models.Guitar.Scales;
using System.Collections.Generic;
using Guitar_Scale_Practice.Models.NoteDisplay;

namespace Guitar_Scale_Practice.Models
{
    [ImplementPropertyChanged]
    public class ScaleHighlightOptions
    {
        private FretAnimator _Animator;

        public int SelectedScaleIdx { get; set; }
        public int SelectedPitchIdx { get; set; }

        public Command ShowScaleCommand { get; private set; }
        public Command ClearBoardCommand { get; private set; }

        public IEnumerable<ScalePreset> Presets => GuitarScaleGenerator.ScaleIntervals.Keys;

        public ScaleHighlightOptions(FretAnimator animator)
        {
            _Animator = animator;
            ShowScaleCommand = new Command(ShowScale);
            ClearBoardCommand = new Command(ClearBoard);
        }

        private void ClearBoard()
        {
            _Animator.ClearAll();
        }

        private void ShowScale()
        {
            _Animator.ShowScale((Pitch)SelectedPitchIdx, (ScalePreset)SelectedScaleIdx);
        }
    }
}