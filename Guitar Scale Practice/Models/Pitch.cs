﻿namespace Guitar_Scale_Practice.Models
{
	//I'll leave it to the presentation layer to decide when
	//the flat version of a note should be used
	public enum Pitch
	{
		C,
		Csharp,
		D,
		Dsharp,
		E,
		F,
		Fsharp,
		G,
		Gsharp,
		A,
		Asharp,
		B,
	}
}